/* 
 * File:   bms_system.h
 * Author: Rob.Davies
 *
 * Created on November 30, 2021, 11:49 AM
 */

#ifndef BMS_SYSTEM_H
#define	BMS_SYSTEM_H

#include <stdbool.h>
#include <stdint.h>
#include <limits.h>

#include "bms_contactor.h"
#include "bms_csc.h"

#ifdef	__cplusplus
extern "C" {
#endif

    #define BMS_SYSTEM_ERROR_CODE_NONE          0U

    #define BMS_SYSTEM_TICKS_ZERO               0U

    #define BMS_SYSTEM_CONTACTOR_COUNT          5
    #define BMS_SYSTEM_CONTACTOR_1              0
    #define BMS_SYSTEM_CONTACTOR_2              1
    #define BMS_SYSTEM_CONTACTOR_3              2
    #define BMS_SYSTEM_CONTACTOR_4              3
    #define BMS_SYSTEM_CONTACTOR_5              4

    #define BMS_SYSTEM_CSC_COUNT                2

    typedef enum
    {
        OperatingModeNormal,
        OperatingModeTest
    } bms_system_operating_mode_t;
    
    typedef struct
    {
        bool bIsInitialised;
        uint16_t u16ErrorCode;
        uint64_t u64Ticks;
        bms_system_operating_mode_t tOperatingMode;
        bms_contactor_t tContactor[BMS_SYSTEM_CONTACTOR_COUNT];
        bms_csc_t tCsc[BMS_SYSTEM_CSC_COUNT];
        
    } bms_system_t;
    
    typedef bms_system_t* p_bms_system_t;
    
    void BMS_SYSTEM_Init(p_bms_system_t ptObject);
    void BMS_SYSTEM_Tick(p_bms_system_t ptObject);
    void BMS_SYSTEM_Check(p_bms_system_t ptObject);
    bms_system_operating_mode_t BMS_SYSTEM_GetOperatingMode(p_bms_system_t ptObject);
    void BMS_SYSTEM_SetOperatingMode(p_bms_system_t ptObject, bms_system_operating_mode_t tOperatingMode);

#ifdef	__cplusplus
}
#endif

#endif	/* BMS_SYSTEM_H */


