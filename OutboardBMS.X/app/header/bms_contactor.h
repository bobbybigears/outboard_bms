/* 
 * File:   bms_contactor.h
 * Author: Rob.Davies
 *
 * Created on November 30, 2021, 2:32 PM
 */

#ifndef BMS_CONTACTOR_H
#define	BMS_CONTACTOR_H

#include <stdbool.h>
#include <stdint.h>

#include "../../mcc_generated_files/system.h"

#ifdef	__cplusplus
extern "C" {
#endif

    #define BMS_CONTACTOR_CONTACTOR_1           LATBbits.LATB8
    #define BMS_CONTACTOR_CONTACTOR_1_ON()      (LATBbits.LATB8 = 1)
    #define BMS_CONTACTOR_CONTACTOR_1_OFF()     (LATBbits.LATB8 = 0)

    #define BMS_CONTACTOR_CONTACTOR_2           LATBbits.LATB9
    #define BMS_CONTACTOR_CONTACTOR_2_ON()      (LATBbits.LATB9 = 1)
    #define BMS_CONTACTOR_CONTACTOR_2_OFF()     (LATBbits.LATB9 = 0)

    #define BMS_CONTACTOR_CONTACTOR_3           LATBbits.LATB13
    #define BMS_CONTACTOR_CONTACTOR_3_ON()      (LATBbits.LATB13 = 1)
    #define BMS_CONTACTOR_CONTACTOR_3_OFF()     (LATBbits.LATB13 = 0)

    #define BMS_CONTACTOR_CONTACTOR_4           LATBbits.LATB14
    #define BMS_CONTACTOR_CONTACTOR_4_ON()      (LATBbits.LATB14 = 1)
    #define BMS_CONTACTOR_CONTACTOR_4_OFF()     (LATBbits.LATB14 = 0)

    #define BMS_CONTACTOR_CONTACTOR_5           LATBbits.LATB15
    #define BMS_CONTACTOR_CONTACTOR_5_ON()      (LATBbits.LATB15 = 1)
    #define BMS_CONTACTOR_CONTACTOR_5_OFF()     (LATBbits.LATB15 = 0)

    typedef enum
    {
        Contactor1,
        Contactor2,
        Contactor3,
        Contactor4,
        Contactor5
    } bms_contactor_id_t;
    
    typedef struct
    {
        bool bIsInitialised;
        bms_contactor_id_t tContactorId;
    } bms_contactor_t;
    
    typedef bms_contactor_t* p_bms_contactor_t;
    
    void BMS_CONTACTOR_Init(p_bms_contactor_t ptObject, bms_contactor_id_t tContactorId);
    void BMS_CONTACTOR_SetOff(p_bms_contactor_t ptObject);
    void BMS_CONTACTOR_SetOn(p_bms_contactor_t ptObject);


#ifdef	__cplusplus
}
#endif

#endif	/* BMS_CONTACTOR_H */

