/* 
 * File:   bms_csc.h
 * Author: Rob.Davies
 *
 * Created on November 30, 2021, 3:36 PM
 */

#ifndef BMS_CSC_H
#define	BMS_CSC_H

#include <stdbool.h>
#include <stdint.h>

#ifdef	__cplusplus
extern "C" {
#endif

    typedef struct
    {
        bool bIsInitialised;
    } bms_csc_t;
    
    typedef bms_csc_t* p_bms_csc_t;
    
    void BMS_CSC_Init(p_bms_csc_t ptObject);


#ifdef	__cplusplus
}
#endif

#endif	/* BMS_CSC_H */

