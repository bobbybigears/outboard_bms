#include "../header/bms_contactor.h"

/****************************************************
 * Private Header Declarations
 ****************************************************/

/****************************************************
 * Public Functions
 ****************************************************/

 void BMS_CONTACTOR_Init(p_bms_contactor_t ptObject, bms_contactor_id_t tContactorId)
 {
     ptObject->tContactorId = tContactorId;   
     BMS_CONTACTOR_SetOff(ptObject);
     ptObject->bIsInitialised = true;
 }
 
 void BMS_CONTACTOR_SetOff(p_bms_contactor_t ptObject)
 {
     switch(ptObject->tContactorId)
     {
         case Contactor1:
         {
             BMS_CONTACTOR_CONTACTOR_1_OFF();
         } break;
         case Contactor2:
         {
             BMS_CONTACTOR_CONTACTOR_2_OFF();
         } break;
         case Contactor3:
         {
            BMS_CONTACTOR_CONTACTOR_3_OFF();
         } break;
         case Contactor4:
         {
            BMS_CONTACTOR_CONTACTOR_4_OFF();
         } break;
         case Contactor5:
         {
            BMS_CONTACTOR_CONTACTOR_5_OFF();
         } break;
         default:
         {
             // TODO
         } break;
     }
 }
    
 void BMS_CONTACTOR_SetOn(p_bms_contactor_t ptObject)
 {
     switch(ptObject->tContactorId)
     {
         case Contactor1:
         {
             BMS_CONTACTOR_CONTACTOR_1_ON();
         } break;
         case Contactor2:
         {
             BMS_CONTACTOR_CONTACTOR_2_ON();
         } break;
         case Contactor3:
         {
            BMS_CONTACTOR_CONTACTOR_3_ON();
         } break;
         case Contactor4:
         {
            BMS_CONTACTOR_CONTACTOR_4_ON();
         } break;
         case Contactor5:
         {
            BMS_CONTACTOR_CONTACTOR_5_ON();
         } break;
         default:
         {
             // TODO
         } break;
     }
 }
 
 /****************************************************
 * Private Functions
 ****************************************************/
 
