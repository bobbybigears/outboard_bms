#include "../header/bms_system.h"

void BMS_SYSTEM_Init(p_bms_system_t ptObject)
{
    uint8_t u8Item = 0;
    
    ptObject->u16ErrorCode = BMS_SYSTEM_ERROR_CODE_NONE;
    ptObject->u64Ticks = BMS_SYSTEM_TICKS_ZERO;
    ptObject->tOperatingMode = OperatingModeNormal;
    
    BMS_CONTACTOR_Init(&ptObject->tContactor[BMS_SYSTEM_CONTACTOR_1], Contactor1);
    BMS_CONTACTOR_Init(&ptObject->tContactor[BMS_SYSTEM_CONTACTOR_2], Contactor2);
    BMS_CONTACTOR_Init(&ptObject->tContactor[BMS_SYSTEM_CONTACTOR_3], Contactor3);
    BMS_CONTACTOR_Init(&ptObject->tContactor[BMS_SYSTEM_CONTACTOR_4], Contactor4);
    BMS_CONTACTOR_Init(&ptObject->tContactor[BMS_SYSTEM_CONTACTOR_5], Contactor5);
    
    for(u8Item=0; u8Item<BMS_SYSTEM_CSC_COUNT; u8Item++)
    {
        BMS_CSC_Init(&ptObject->tCsc[u8Item]);
    }
    
    ptObject->bIsInitialised = true;
}

void BMS_SYSTEM_Tick(p_bms_system_t ptObject)
{
    if(ptObject->bIsInitialised)
    {
       ptObject->u64Ticks++;

       if(ptObject->u64Ticks > (ULLONG_MAX - 1))
       {
           ptObject->u64Ticks = BMS_SYSTEM_TICKS_ZERO;
       }
    }
}
 
bms_system_operating_mode_t BMS_SYSTEM_GetOperatingMode(p_bms_system_t ptObject)
{
    return ptObject->tOperatingMode;
}

void BMS_SYSTEM_SetOperatingMode(p_bms_system_t ptObject, bms_system_operating_mode_t tOperatingMode)
{
    if(ptObject->bIsInitialised)
    {
        ptObject->tOperatingMode = tOperatingMode;
    }
}
 
void BMS_SYSTEM_Check(p_bms_system_t ptObject)
{
   if((ptObject->bIsInitialised) && (ptObject->tOperatingMode == OperatingModeNormal))
   {
       // TODO
   }
}
